# Async load and events

* Micro Frontends may load in any order.
    * Make sure that events are not lost.
    * Can use attributes/props to set initial values.

Note:
