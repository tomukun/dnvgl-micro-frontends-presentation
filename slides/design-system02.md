## core-button

```typescript
import { customElementToReact } from '@spk/custom-element-to-react';
// @ts-ignore
import darkColors from '../../style/button/dark';
// @ts-ignore
import defaultColors from '../../style/button/default';
// @ts-ignore
import highContrastColors from '../../style/button/high-contrast';
import { isSet } from '../common/attribute';
import createStyle from '../common/createStyle';
import guid from '../common/guid';
import { Theme, ThemeValue } from '../common/theme';

export const name = 'core-button';

export enum TypeValue {
  primary = 'primary',
  secondary = 'secondary',
  supportive = 'supportive',
}

export type Type =
  | TypeValue.primary
  | TypeValue.secondary
  | TypeValue.supportive;

enum Observed {
  buttonType = 'button-type',
  disabled = 'disabled',
  mini = 'mini',
  spinner = 'spinner',
}

const buildClassNames = (buttonType: Type, mini = false, spinner = false) => {
  return `form-item__button form-item__button--${buttonType}${
    spinner ? ' form-item__button--spinner' : ''
  }${mini ? ' form-item__button--mini' : ''}`;
};

export class CoreButton extends HTMLElement {
  public b: HTMLButtonElement;

  static get observedAttributes() {
    return [...Object.values(Observed)];
  }

  constructor() {
    super();
    this.createContent = this.createContent.bind(this);
  }

  public connectedCallback() {
    const shadow = this.attachShadow({
      mode: 'open',
    });

    const colorTheme = this.getAttribute('theme') as Theme;
    if (colorTheme === ThemeValue.dark) {
      shadow.appendChild(createStyle(darkColors));
    } else if (colorTheme === ThemeValue.highContrast) {
      shadow.appendChild(createStyle(highContrastColors));
    } else {
      shadow.appendChild(createStyle(defaultColors));
    }
    this.b = this.createContent();
    shadow.append(this.b);
  }

  attributeChangedCallback() {
    const disabled = isSet(this.getAttribute(Observed.disabled));
    try {
      this.b.disabled = disabled;
      this.setAttribute('aria-disabled', `${disabled}`);
      this.b.setAttribute(
        'class',
        buildClassNames(
          this.getAttribute(Observed.buttonType) as Type,
          isSet(this.getAttribute(Observed.mini)),
          isSet(this.getAttribute(Observed.spinner))
        )
      );
    } catch (e) {}
  }

  public createContent() {
    const b = document.createElement('button');
    const disabled = isSet(this.getAttribute(Observed.disabled));

    b.setAttribute('id', this.getAttribute('id') || guid());
    b.setAttribute('type', this.getAttribute('type') || 'button');
    b.setAttribute(
      'class',
      buildClassNames(
        this.getAttribute(Observed.buttonType) as Type,
        isSet(this.getAttribute(Observed.mini)),
        isSet(this.getAttribute(Observed.spinner))
      )
    );

    b.textContent = this.textContent;
    b.onclick = this.onclick;
    b.disabled = disabled;
    this.setAttribute('aria-disabled', `${disabled}`);
    b.setAttribute('style', 'width: 100%;');

    return b;
  }
}

export const React = customElementToReact(CoreButton);

export const define = () => customElements.define(name, CoreButton);
```
