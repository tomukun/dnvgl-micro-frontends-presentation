# Micro Frontends


Note:
Core ideas:
- Be technology agnostic
- Isolate team code
- Strive for standards (HTML, JS, ES20*)
- Build a flexible application