## The ES Module specification

```html
<script type="module" src="awesome-component.js"></script>
```

```html
<script type="module">
  import "awesome-component.js";
</script>
```

```html
<script type="module">
  import { awesomeComponent } from "@awesome-things/awesome-component";
</script>
```

```html
<awesome-component />
```

Note:
https://www.webcomponents.org/specs

The ES Module specification defines the inclusion and reuse of JS documents in other JS documents.

You can define the interface of your custom element in a JS file which is then included with an type="module" attribute.
ES Module files are merged into one file client side or can be rolled up into single packages ahead of time.
