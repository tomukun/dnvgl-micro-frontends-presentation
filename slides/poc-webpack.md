# Dev config
```javascript
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = require('./webpack.common')({
    mode: 'development',
    devServer: {
        ...
    },
    output: {
        filename: 'testcomp-react.js',
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: 'node_modules/html-webpack-template/index.ejs',
            title: 'saksflyt-spike-webcomponents-react',
            bodyHtmlSnippet: '<test-comp-react></test-comp-react>'
        }),
    ],
});
```
