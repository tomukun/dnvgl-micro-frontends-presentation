```javascript
class Component extends HTMLElement {
    constructor() {
        super();
        const shadowRoot = this.attachShadow({mode: 'open'});
        shadowRoot.appendChild(...);
    }
    connectedCallback() {}
    disconnectedCallback() {}
    static get observedAttributes() {
        return ['some-attribute', 'some-other-attribute'];
    }
    attributeChangedCallback(attrName, oldVal, newVal) {}
}
window.customElements.define('component', Component);
```

Note:
Enkleste mulige JS-eksempel
Shadow DOM provides a way for an element to own, render, and style a chunk of DOM that's separate from the rest of the page. Heck, you could even hide away an entire app within a single tag.
