# Old code base

```none
        Language     files  blank  comment    code
        ------------------------------------------
        ActionScript  1910  30762     4466  110012
        Java           999  21647     2502   89159
        MXML           519  10008     1529   61656
        Maven           27    274      108    7374
        CSS              3    356      245    2003
        JavaScript       6    208      226    1510
        HTML            15    118        0     891
        Ant              1     70       52     419
        JSP              2     21        0     121
        Markdown         1     14        0      42
        ------------------------------------------
        SUM:           348  63478     9128  273187
```

Note:
Generert med cloc (Count Lines of Code)
https://github.com/AlDanial/cloc
