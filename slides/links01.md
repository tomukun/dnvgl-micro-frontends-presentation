# Links

- [micro-frontends.org/](https://micro-frontends.org/)
- [webcomponents.org](https://www.webcomponents.org)
- [webcomponents.org/specs](https://www.webcomponents.org/specs)
- [webcomponents.org/polyfills](https://www.webcomponents.org/polyfills)
- [developers.google.com/web/fundamentals/web-components/customelements](https://developers.google.com/web/fundamentals/web-components/customelements)
