# Old vs new

```none
Browser → Flash Player → Flex →
BlazeDS →
JVM → Java/Spring/Hibernate → MS SQL Server
```

vs

```none
Browser → HTML/JS/CSS →
HTTP →
JVM → Java/Spring/Hibernate → MS SQL Server
```
