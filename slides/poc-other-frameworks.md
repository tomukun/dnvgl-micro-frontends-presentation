# Other frameworks?

- Polymer
- Elm
- Reason

Note:

- Polymer - Nok en markup-variant. Kan kun brukes for WC.
- Elm - Har nå full støtte via https://github.com/thread/elm-web-components
- Reason - Fant ingenting der ute, men kan kanskje funke med https://reasonml.github.io/reason-react/?
