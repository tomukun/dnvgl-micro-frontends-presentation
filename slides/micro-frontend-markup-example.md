```html
<puma-menu>
    <puma-search />
</puma-menu>
<noekkelinformasjon-web />
<div class="saksbehandling__innhold">
  <div class="saksbehandling__section">
    <puma-sak />
    <puma-meldinger />
    <puma-personlogg />
  </div>
  <div class="saksbehandling__oppgavevisning">
    <oppsummering-til-saksbehandler />
    <info-og-rutiner />
    <puma-filter />
    <nav-data />
    <peregnet-pensjon />
  </div>
  <div class="saksbehandling__section">
    <konsekvens-av-godkjenning />
    <saksbehandling-kommentarer />
    <saksbehandling-dokumenter />
  </div>
</div>
```
Note:
- simple html stiching it all toughether
- each custom element is microfrontend with their own backends and database tables