# Custom Events

```typescript
events.triggerEvent('valgtSak:updated', { medlemId, sakId });
```

```typescript
export default {
    triggerEvent: (id: string, payload: any) => {
        window.dispatchEvent(new CustomEvent(id, {
            bubbles: true,
            detail: {
                payload: {
                    ...payload,
                    originator: APPLICATION_NAME,
                },
            },
        }));
    },
};
```

Note:
new CustomEvent(id, payload)
