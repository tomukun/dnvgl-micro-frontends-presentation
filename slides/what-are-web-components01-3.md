## The HTML Template specification

```html
<template id="awesome-template">
  <img src="" alt="Awesome image" />
  <div class="comment"></div>
</template>
```

```javascript
const template = document.querySelector("#awesome-template");
template.content.querySelector("img").src = "awesome-image.png";
const clone = document.importNode(template.content, true);
document.body.appendChild(clone);
```

Note:
https://www.webcomponents.org/specs
The template element is used to declare fragments of HTML that can be cloned and inserted in the document by script.

Templates can be placed anywhere in <head>, <body> or <frameset> and can contain any content that is allowed in those elements.

Content between <template></template> tags:

- Will not render until it is activated
- Has no effect on other parts of the page - scripts won’t run, images won’t load, audio won’t play - until activated
- Will not appear in the DOM
