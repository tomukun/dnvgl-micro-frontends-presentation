# Micro Frontends

## About this project

Slides are rendered using [reveal.js](https://github.com/hakimel/reveal.js/). It's been set up by doing the following:

- `git clone https://github.com/hakimel/reveal.js.git`
- `cp -r reveal.js <navn på prosjekt>`
- `npm install`

## How to run the presentation?

1. `git clone ...`
2. `npm install`
3. `./start.sh`
4. Navigate to http://localhost:8080

## Functions

As the presentations is running:

- `f` for fullscreen
- `s` for presenter mode
- `alt/ctrl + click` for zoom
- `esc` for slide overview

Every slide has it's own unique URL within the presentation.

## How was the presentation put together?

- Changed the `README.md`.
- Changed the `index.html`.
- Individual markdown-based slides added under `./slides`.
- Some custom styling added under `./slides`.
- Added `./start.sh`.
- Added various images under `./slides/images`
